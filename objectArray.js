const users = [
  {
    name: "Mike Shell",
    username: "mikeBlueShell",
    email: "mikeyShelle1@gmail.com",
    password: "iammikey1999",
    isActive: true,
    dateJoined: "August 8, 2011",
  },
  {
    name: "Jake Janella",
    username: "jjnella99",
    email: "jakejanella_99@gmail.com",
    password: "jakiejake12",
    isActive: true,
    dateJoined: "January 14, 2015",
  },
];

console.log(users[0]);

users[0].email = "mikeKingOfShells@gmail.com";
users[1].email = "janellajakeArchitect@gmail.com";

users.push({
  name: "James Jameson",
  username: "iHateSpidey",
  email: "jamesJjameson@gmail.com",
  password: "spideyisamenace64",
  isActive: true,
  dateJoined: "February 14, 2000",
});

class User {
  constructor(name, username, email, password) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.isActive = true;
    this.dateJoined = Date();
  }
}
let newUser1 = new User(
  "Kate Middletown",
  "notTheDuchess",
  "seriouslyNotDuchess@gmail.com",
  "notRoyaltyAtAll"
);

users.push(newUser1);

function login(username, password) {
  const authUser = users.find(
    (user) => user.username == username && user.password === password
  );
  if (authUser) {
    console.log(
      `%c Thank you for logging in, ${authUser.username}`,
      "color:green"
    );
    return authUser;
  }
  console.log("%c Invalid login credentials", "color:red");
}

let courses = [];

class Course {
  constructor(id, name, description, price) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.isActive = true;
  }
}

const postCourse = (id, name, description, price) => {
  const course = new Course(id, name, description, price);
  courses.push(course);
  alert(`You have created ${course.name}. The price is ${course.price}`);
};

const getSingleCourse = (id) => courses.find((course) => course.id === id);

// const deleteCourse = (id) => {
//   courses = courses.filter((course) => course.id !== id);
// };

const deleteCourse = () => courses.pop();
